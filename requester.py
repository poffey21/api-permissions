import json

import requests
from envparse import Env
import os


env = Env()
if os.path.isfile('.env'):
    env.read_envfile('.env')


def get_all_values(nested_dictionary, previous=None):
    all = []
    for key, value in nested_dictionary.items():
        v = ''
        if previous:
            v += previous + '/'
        v += key
        all.append(v)
        if type(value) is dict:
            all.extend(get_all_values(value, v))
    return all


def main():
    """
    Run through all scenarios where you would want to use
    a CI_JOB_TOKEN
    """
    job_id = env('CI_JOB_ID')
    pipeline_id = env('CI_PIPELINE_ID')
    project_id = env('CI_PROJECT_ID')
    job_token = env('CI_JOB_TOKEN')
    branch = env('CI_COMMIT_BRANCH')

    # Using this example:
    # https://gitlab.com/gitlab-org/gitlab/-/issues/32559#workaround
    url = f'https://gitlab.com/api/v4/projects/{project_id}/trigger/pipeline'
    child = requests.post(url, data={'token': job_token, 'ref': branch}).json()
    child_pipeline_id = child['id']

    scenarios = {
        'user': {},
        'version': {},
        'applications': {},
        'broadcast_messages': {},
        'users': {},
        'projects': {
            f'{project_id}': {
                'jobs': {
                    f'{job_id}': {}
                },
                'pipelines': {
                    f'{pipeline_id}': {},
                    f'{child_pipeline_id}': {},
                },
                'repository': {
                    'tree': {},
                    'branches': {},
                },
            }
        },
    }
    headers = {
        'JOB-TOKEN': job_token
    }

    for url in get_all_values(scenarios, 'https://gitlab.com/api/v4'):
        r = requests.get(url, headers=headers)
        if not r.ok:
            try:
                if 'message' in r.json():
                    print(url, r.status_code, '--> (via message)', r.json().get('message'))
                elif 'error' in r.json():
                    print(url, r.status_code, '--> (via error)', r.json().get('error'))
                else:
                    print(url, r.status_code, '-->', r.json())
            except json.decoder.JSONDecodeError:
                print(url, r.status_code, '-->', 'Unable to get an error message')
        else:
            print(url, r.status_code)


if __name__ == '__main__':
    main()
